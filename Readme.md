# Aplicando TDD e padrões de testes

* [Aplicando TDD e padrões de testes]()
  * [Como é o TDD circle of Lyfe e preparação do código]()
  * [Implementando preparação do mock]()
  * [Validar os valores de retorno do método]()
  * [Certifique seu conhecimento]()



## Aplicando TDD e Padrões de Testes no Desenvolvimento de Aplicativos Android

<div align="center">
   <img src="./assets/tdd.png"/>
</div>

* 1 - **Test Fails** : Aqui é criada a rotina de testes porém o teste irá falhar pois ainda é necessário implementar a rotina que será testada.
* 2 - **Test Passes** : Aqui é onde a feature é implementada até que o Test Fails funcione corretamente.
* 3 - **Refactor** : Aqui é onde acontece a refatoração da feature que acabou de passar 

## Certifique seu Conhecimento

### Qual é a ordem correta do fluxo de desenvolvimento com TDD?
* [x] Testes falhar - Testes passar - Refatorar

### Para criação de um método para teste unitário, devemos utilizar qual anotação?
* [x] @Test

### Para conseguirmos criar e executar testes unitários, devemos criá-los em um pacote especifico. Qual seria o nome do pacote?
* [x] Test

### Em alguns momentos necessitamos utilizar o Robolectric para obter o contexto da nossa aplicação. Onde são executados os testes unitários utilizando o Robolectric?
* [x] JVM

### Sobre TDD (test driven development):
* [x] Refere-se a uma técnica de programação cujo principal objetivo é escrever um código funcional limpo, a partir de um teste que tenha falhado.

### Para fazer a asserção de valores numéricos em testes unitários, utilizamos o método:
* [x] assetEquals()

### Para que serve o objeto mock/fake em testes?
* [x] Simular o comportamento de um objeto real

### Em qual momento no desenvolvimento de software deve ser feito a criação dos testes seguindo o TDD?
* [x] Antes de realizar qualquer implementação de código de projeto

### Qual é a ordem correta do fluxo de desenvolvimento com TDD ?
* [x] Testes falhar - Testes Passar - Refatorar

### Quais os principais benefícios do TDD ?
* [x] O Código será mais limpo e simples; A confiabilidade do Código será maior; Favorece o processo de documentação

### Para uma boa organização do escopo de cada método de testes, seguimos uma estrutura lógica de :
* [x] Preparar o método com tudo que ele precisa para em seguida ser validado.

